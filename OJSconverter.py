#!/usr/bin/env python
# coding: utf-8

import sys
import os
import argparse
from xml.sax.saxutils import escape
import mimetypes
import pandas as pd
try:
    from flask import flash
except:
    pass


# ------------------------ #
# --- globals          --- #
# ------------------------ #
MISSING_DATA = 'MissingData'
UPLOAD_USER_OJS = 'prod'
# ------------------------ #



def DoiGen(v, i, p, DOIliste, DOIpre = 'X'):
    try:
        doiGen = v, i, p
        DOI = DOIpre + ".%s.%s.%s" % (v, i, p)
        if DOI in DOIliste:
            for i in ['b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']:
                DOI += i
                if DOI in DOIliste:
                    DOI = DOI[:-1]
                    print(DOI)
                else:
                    DOIliste.add(DOI)
                    return DOI
        else:
            DOIliste.add(DOI)
            return DOI        
    except:
        DOI = MISSING_DATA
        try:
            flash('Error Generating DOI')
        except:
            print('Error Generating DOI')
        return DOI



# Copyrightholder

def CopyrightholderStrDE(x, VNLNDictDE):
    k = str()
    ch = [VNLNDictDE[i] for i in x.split(', ')]
    if len(ch) == 1:
        k = str(ch[0])
    else:
        for i in range(len(ch)):
            if i < len(ch) - 1:
                k = k+ch[i] + ', '
            else:
                k = k[:-2] + ' und ' + ch[i]
    return k


def CopyrightholderStrEN(x, VNLNDictEN):
    k = str()
    ch = [VNLNDictEN[i] for i in x.split(', ')]
    if len(ch) == 1:
        k = str(ch[0])
    else:
        for i in range(len(ch)):
            if i < len(ch) - 1:
                k = k + ch[i] + ', '
            else:
                k = k[:-2] + ' and ' + ch[i]
    return k



# SUBMISSION

def submission(Year, submissionID, articleLanguage, PDFfile, HTMLfile, supplementalHTMLfiles, PDFfileTransl, HTMLfileTransl, URLpre, DatePub, xml):

    Artikeltext = 'Article Text'  # Genre
    URLpath = URLpre + os.path.splitext(PDFfile)[0] + '/'

    if URLpre != MISSING_DATA:
        if PDFfile != MISSING_DATA:
            pdfID = submissionID
            xml.write('\n         <submission_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="production_ready" id="' + str(pdfID) + '" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')
            xml.write('\n           <revision number="1" genre="' + Artikeltext + '" filename="' + PDFfile + '" viewable="true" date_modified="' + DatePub + '" filetype="application/pdf" uploader="' + UPLOAD_USER_OJS + '">')
            xml.write('\n             <name locale="' + articleLanguage + '">' + PDFfile.split('/')[-1] + '</name>')
            xml.write('\n             <href src="' + URLpath + PDFfile + '" />')
            xml.write('\n           </revision>')
            xml.write('\n         </submission_file>')
            submissionID += 1

        if HTMLfile != MISSING_DATA:
            htmlID = submissionID
            xml.write('\n         <submission_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="proof" id="' + str(htmlID) + '" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')
            xml.write('\n           <revision number="1" genre="' + Artikeltext + '" filename="' + HTMLfile + '" viewable="true" date_modified="' + DatePub + '" filetype="text/html" uploader="' + UPLOAD_USER_OJS + '">')
            xml.write('\n             <name locale="' + articleLanguage + '">' + HTMLfile.split('/')[-1] + '</name>')
            xml.write('\n             <href src="' + URLpath + HTMLfile + '" />')
            xml.write('\n           </revision>')
            xml.write('\n         </submission_file>')
            submissionID += 1
        
        if supplementalHTMLfiles != MISSING_DATA:
            mimetypes.init()
            supplementalHTMLfilesSplitted = supplementalHTMLfiles.split('||')
            for supplementalHTMLfile in supplementalHTMLfilesSplitted: 
                supplementalHTMLfileSplitted = supplementalHTMLfile.split('::')
                supplementalHTMLfileName = supplementalHTMLfileSplitted[0]
                if ( len(supplementalHTMLfileSplitted) > 1 ) and ( supplementalHTMLfileSplitted[1] != '' ):  # genre = Author Image || Image
                    supplementalHTMLfileGenre = supplementalHTMLfileSplitted[1]
                else:
                    supplementalHTMLfileGenre = 'Image'
                supplementalHTMLfileMimeType = mimetypes.guess_type( supplementalHTMLfileName )[0]
                xml.write('\n         <supplementary_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="dependent" id="' + str(submissionID) + '" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')
                xml.write('\n           <revision number="1" genre="' + supplementalHTMLfileGenre + '" filename="' + supplementalHTMLfileName + '" viewable="false" date_uploaded="' + DatePub + '" date_modified="' + DatePub + '" filetype="' + supplementalHTMLfileMimeType + '" uploader="' + UPLOAD_USER_OJS + '">')
                xml.write('\n             <name locale="' + articleLanguage + '">' + supplementalHTMLfileName.split('/')[-1] + '</name>')
                xml.write('\n             <submission_file_ref id="' + str(htmlID) + '" revision="1"/>')
                xml.write('\n             <href src="' + URLpath + supplementalHTMLfileName + '" />')
                xml.write('\n           </revision>')
                xml.write('\n         </supplementary_file>')
                submissionID += 1

        if PDFfile != MISSING_DATA:
            xml.write('\n         <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="false" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')
            xml.write('\n           <id type="internal" advice="ignore">' + str(submissionID) + '</id>')
            xml.write('\n           <name locale="' + articleLanguage + '">PDF</name>')
            xml.write('\n           <seq>0</seq>')
            xml.write('\n           <submission_file_ref id="' + str(pdfID) + '" revision="1" />')
            xml.write('\n         </article_galley>')
            submissionID += 1

        if HTMLfile != MISSING_DATA:
            xml.write('\n         <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="false" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')
            xml.write('\n           <id type="internal" advice="ignore">' + str(submissionID) + '</id>')
            xml.write('\n           <name locale="' + articleLanguage + '">HTML</name>')
            xml.write('\n           <seq>1</seq>')
            xml.write('\n           <submission_file_ref id="' + str(htmlID) + '" revision="1" />')
            xml.write('\n         </article_galley>')
            submissionID += 1
        
        return submissionID



# AUTOREN

def authors(Autoren, Authors, xml):
    xml.write('\n         <authors xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="pkp.sfu.ca native.xsd">')
    if len(Autoren) > 0:
        for autor in Autoren:
            try:
                Autor(autor, Authors, xml)
            except:
                Autor(autor, Authors, xml)
                print('Fehler beim Schreiben von Autor: ', Autoren)
    else:
        Autor(Autoren, Authors, xml)
    # schliesst die Klammer
    xml.write('\n         </authors>')


def Autor(autor, Authors, xml):
    EMail = 'redaktion@tatup.de'
    AFFI = ' '
    CN = ' '
    EMail = ' '
    FNde = escape( Authors[Authors.index == autor]['Givenname'][0] )
    LNde = escape( Authors[Authors.index == autor]['Familyname'][0] )
    AFFIde = escape( Authors[Authors.index == autor]['Affiliation'][0] )
    FNen = escape( Authors[Authors.index == autor]['GivennameEN'][0] )
    LNen = escape( Authors[Authors.index == autor]['FamilynameEN'][0] )
    AFFIen = escape( Authors[Authors.index == autor]['Affiliation'][0] )
    CN = escape( Authors[Authors.index == autor]['Land'][0] )
    EMail = escape( Authors[Authors.index == autor]['E-Mail'][0] )
    ORCID = escape( Authors[Authors.index == autor]['ORCID'][0] )

    if Authors[Authors.index == autor]['PrimaryContact'][0] == 'ok':
        primary_contact = ' primary_contact="true"'
        #print('primary_contact: ',autor)
    else:
        primary_contact = ' '
        #print('kein PContact:', autor)

    #print('FN: ',FNde,' LN: ',LNde)

    xml.write('\n           <author' + primary_contact + ' include_in_browse="true" user_group_ref="Author">')
    xml.write('\n              <givenname locale="de_DE">' + FNde + '</givenname>')
    xml.write('\n              <givenname locale="en_US">' + FNen + '</givenname>')
    xml.write('\n              <familyname locale="de_DE">' + LNde + '</familyname>')
    xml.write('\n              <familyname locale="en_US">' + LNen + '</familyname>')
    if AFFIde != MISSING_DATA:
        xml.write('\n              <affiliation locale="de_DE">' + AFFIde + '</affiliation>')
    if AFFIen != MISSING_DATA:
        xml.write('\n              <affiliation locale="en_US">' + AFFIen + '</affiliation>')
    if CN != MISSING_DATA:
        xml.write('\n              <country>' + CN + '</country>')
    xml.write('\n              <email>' + EMail + '</email>')
    if ORCID != MISSING_DATA:
        xml.write('\n              <url>' + ORCID + '</url>')
    xml.write('\n           </author>')



# ARTIKEL

def article(Authors, TitleDe, TitleEn, SubtitleDe, SubtitleEn, DatePub, articleLanguage, SectRef, ArtNr, DOI, abstractDE, abstractEN, keywordsDE, keywordsEN, pages, CopyrightholderDE, CopyrightholderEN, Autoren, Year, submissionID, PDFfile, HTMLfile, supplementalHTMLfiles, PDFfileTransl, HTMLfileTransl, URLpre, xml):

    xml.write('\n     <article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" locale="' + str(articleLanguage) + '" date_submitted="' + DatePub + '" stage="production" date_published="' + str(DatePub) + '" section_ref="' + str(SectRef) + '" seq="' + str(ArtNr) + '" access_status="0">')
    xml.write('\n         <id type="internal" advice="ignore">' + str(ArtNr) + '</id>')

    if DOI != MISSING_DATA:
        xml.write('\n         <id type="doi" advice="update">' + str(DOI) + '</id>')

    xml.write('\n         <title locale="de_DE">' + escape( str(TitleDe) ) + '</title>')
    xml.write('\n         <title locale="en_US">' + escape( str(TitleEn) ) + '</title>')

    if SubtitleDe != MISSING_DATA:
        xml.write('\n         <subtitle locale="de_DE">' + escape( str(SubtitleDe) ) + '</subtitle>')
    if SubtitleEn != MISSING_DATA:
        xml.write('\n         <subtitle locale="en_US">' + escape( str(SubtitleEn) ) + '</subtitle>')

    abstractWrite(abstractDE, abstractEN, xml)

    xml.write('\n         <licenseUrl>http://creativecommons.org/licenses/by/4.0</licenseUrl>')
    xml.write('\n         <copyrightHolder locale="de_DE">' + escape( CopyrightholderDE ) + '</copyrightHolder>')
    xml.write('\n         <copyrightHolder locale="en_US">' + escape( CopyrightholderEN ) + '</copyrightHolder>')
    xml.write('\n         <copyrightYear>' + str(Year) + '</copyrightYear>')

    keywords(keywordsDE, keywordsEN, xml)

    authors(Autoren, Authors, xml)
    submissionID = submission(Year, submissionID, articleLanguage, PDFfile, HTMLfile, supplementalHTMLfiles, PDFfileTransl, HTMLfileTransl, URLpre, DatePub, xml)

    xml.write('\n        <pages>' + pages + '</pages>')
    xml.write('\n     </article>')
    
    return submissionID


def StartTag(xml):
    xml.write('<?xml version="1.0" encoding="utf-8"?>')
    xml.write('\n<issue xmlns="http://pkp.sfu.ca" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" published="0" current="0" access_status="1" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')


def PreArticlesA(Volume, Nr, Year, JournalTitleDE, JournalTitleEN, IssueID, DatePub, xml):
    StartTag(xml)
    IssIdenttag(Volume, Nr, Year, JournalTitleDE, JournalTitleEN, IssueID,xml)
    # xml.write('\n  <date_published>'+DatePub+'</date_published>')  # 
    xml.write('\n  <last_modified>'+DatePub+'</last_modified>')
    xml.write('\n  <sections>')


def PreArticlesB(xml):
    xml.write('\n  <articles xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">')


def PostArticles(xml):
    xml.write('\n  </articles>')
    xml.write('\n</issue>')


def IssIdenttag(Volume, Nr, year, JournalTitleDE, JournalTitleEN, IssueID, xml):
    xml.write('\n  <id type="internal" advice="ignore">' + IssueID + '</id>')
    # xml.write('\n  <description locale="de_DE"></description>')
    # xml.write('\n  <description locale="en_US"></description>')
    xml.write('\n  <issue_identification>')
    xml.write('\n    <volume>' + Volume + '</volume>')
    xml.write('\n    <number>' + Nr + '</number>')
    xml.write('\n    <year>' + year + '</year>')
    xml.write('\n    <title locale="de_DE">' + escape( JournalTitleDE ) + '</title>')
    xml.write('\n    <title locale="en_US">' + escape( JournalTitleEN ) + '</title>')
    xml.write('\n  </issue_identification>')


def IssueGalleyEmpty(xml):
    xml.write('\n  </sections>')
    xml.write('\n  <issue_galleys xsi:schemaLocation="http://pkp.sfu.ca native.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>')


def IssueGalley(year, IssID, IssFileName, DatePub, xml):
    xml.write('\n  </sections>')

    xml.write('\n  <issue_galleys xsi:schemaLocation="http://pkp.sfu.ca native.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">')
    xml.write('\n    <issue_galley locale="de_DE">')
    xml.write('\n      <label>PDF</label>')
    xml.write('\n        <issue_file>')
    # xml.write('\n          <name locale="de_DE">' + IssFileName + '</name>')
    xml.write('\n          <file_name>' + IssFileName + '</file_name>')
    xml.write('\n          <file_type>application/pdf</file_type>')
    xml.write('\n          <file_size>0</file_size>')
    xml.write('\n          <content_type>1</content_type>')
    xml.write('\n          <original_file_name>' + IssFileName.split('/')[-1] + '</original_file_name>')
    xml.write('\n          <date_uploaded>' + DatePub + '</date_uploaded>')
    xml.write('\n          <date_modified>' + DatePub + '</date_modified>')
    xml.write('\n          <embed encoding="base64">')
    # xml.write('\n            <href src="' + IssFileName + '"/>')
    xml.write('\n          </embed>')
    xml.write('\n        </issue_file>')
    xml.write('\n    </issue_galley>')
    xml.write('\n  </issue_galleys>')


def sections(Englisch, KurzEN, Deutsch, KurzDE, Seq, xml):
    xml.write('\n    <section ref="' + KurzEN + '" seq="' + str(Seq + 1) + '" editor_restricted="0" meta_indexed="1" meta_reviewed="0" abstracts_not_required="1" hide_title="0" hide_author="0" abstract_word_count="0">')
    xml.write('\n      <id type="internal" advice="ignore">' + str(Seq + 1) + '</id>')
    xml.write('\n      <abbrev locale="de_DE">' + escape( KurzDE ) + '</abbrev>')
    xml.write('\n      <abbrev locale="en_US">' + escape( KurzEN ) + '</abbrev>')
    xml.write('\n      <title locale="de_DE">' + escape( Deutsch ) + '</title>')
    xml.write('\n      <title locale="en_US">' + escape( Englisch ) + '</title>')
    xml.write('\n    </section>')


def keywords(keywordsDE, keywordsEN, xml):
    if keywordsDE[0] != MISSING_DATA:
        xml.write('\n         <keywords locale="de_DE">')
        for i in keywordsDE:
            xml.write('\n           <keyword>' + escape( str(i.split('<')[0]).strip() ) + '</keyword>')
        xml.write('\n         </keywords>')
    if keywordsEN[0] != MISSING_DATA:
        xml.write('\n         <keywords locale="en_US">')
        for i in keywordsEN:
            xml.write('\n           <keyword>' + escape( str(i.split('<')[0]).strip() ) + '</keyword>')
        xml.write('\n         </keywords>')


def abstractWrite(abstractDE, abstractEN, xml):
    # print(abstract)
    if abstractDE != MISSING_DATA:
        xml.write('\n         <abstract locale="de_DE">' + escape( str(abstractDE) ) + '</abstract>')
    if abstractEN != MISSING_DATA:
        xml.write('\n         <abstract locale="en_US">' + escape( str(abstractEN) ) + '</abstract>')



# Fur den Import

def DictionaryMaker(Authors, Dropdown):
    VNLNDictDE = pd.Series(Authors.Givenname.values + ' ' + Authors.Familyname.values, index=Authors.index).to_dict()
    VNLNDictEN = pd.Series(Authors.GivennameEN.values + ' ' + Authors.FamilynameEN.values, index=Authors.index).to_dict()
    SectDict = pd.Series(Dropdown.KurzDE.values, index=Dropdown.Englisch).to_dict()
    return VNLNDictDE, VNLNDictEN, SectDict


def AutorCheck(AuthorKeys, Authors):
    AutorC = []
    for i in AuthorKeys:
       # print(i)
        try:
            for j in i.split(', '):
                Authors[Authors.index == j]['Familyname'][0]
                AutorC = 'No errors in the author link.'
        except:
            AutorC = str(i) + 'Error in the author link.'
            #AutorC += [['Author is missing in in article: Name: ']] #String Meldung des Fehlers
    return AutorC        


def ExcelImport(filename):
    # try:
    print('Importing', filename)
    Journal = pd.read_excel(filename, sheet_name='Journal', converters={'Volume': str, 'Number': str, 'Year': str, 'DatePublished': str, 'Server': str, 'DOI-pre': str}).fillna(MISSING_DATA)
    Articles = pd.read_excel(filename, sheet_name='Articles', converters={'Startpage': str, 'Endpage': str, 'CopyrightYear': str}).applymap(lambda x: x.strip() if isinstance(x, str) else x).fillna(MISSING_DATA)
    Authors = pd.read_excel(filename, sheet_name='Authors', index_col='AuthorKey', converters={'AuthorKey': str}).fillna(MISSING_DATA).applymap(lambda x: x.strip() if isinstance(x, str) else x)
    Dropdown = pd.read_excel(filename, sheet_name='Dropdown')
    Journal.DatePublished[0] = Journal.DatePublished[0].split()[0]
    Authors.drop_duplicates(inplace=True)
    ArticleSpalten = ['Section', 'AuthorKey', 'Language', 'TitleDE', 'SubtitleDE', 'AbstractDE', 'AbstractEN', 'TitleEN', 'SubtitleEN', 'KeywordsDE', 'KeywordsEN', 'Startpage', 'Endpage', 'copyrightHolder', 'CopyrightYear', 'DOI', 'FilePDF', 'FileHTML', 'supplementalFilesHTML']

    # print(Journal.columns, Journal['DOI-pre'])
    for i in ArticleSpalten:
        if i not in Articles.columns:
            try:
                flash( i + 'Spalte missing.')
            except:
                print( i + 'Spalte missing.' )
    # except:
    #     print('Import Error :(')
    return Journal, Articles, Authors, Dropdown


def XMLersteller(Journal, Articles, Authors, Dropdown, XMLFilename):
    DOIliste = set()
    xml = open( XMLFilename, 'w', encoding = 'utf-8' )
    VNLNDictDE, VNLNDictEN, SectDict = DictionaryMaker(Authors, Dropdown)

    # HIER STARTET DAS EIGENTLICHE PROGRAMM
    try:
        URLpre = Journal.Server[0]
    except:
        URLpre = MISSING_DATA
        DOIpre = MISSING_DATA
    
    Vol = Journal.Volume[0]
    Nr = Journal.Number[0]
    Year = Journal.Year[0]
    print(Year)
    
    try:
        DOIpre = Journal['DOI-pre'][0]
        print(DOIpre)
    except:
        DOIpre = MISSING_DATA
        print('DoiPre not accepted', DOIpre)
    
    DatePub = Journal.DatePublished[0]
    IssFileName = URLpre + Journal.IssueFilename[0]
    JournalTitleDE = Journal.TitleDE[0]
    JournalTitleEN = Journal.TitleEN[0]
    IssueID = '1'

    PreArticlesA(Vol, Nr, Year, JournalTitleDE, JournalTitleEN, IssueID, DatePub, xml)

    for i in Dropdown.index:
        sections(Dropdown.Englisch[i], Dropdown.KurzEN[i], Dropdown.Deutsch[i], Dropdown.KurzDE[i], i, xml)
    IssueGalleyEmpty(xml)
    # IssueGalley(Year, IssueID, IssFileName, DatePub, xml)  

    PreArticlesB(xml)
    outputArt = []
    submissionID = 1
    for A in Articles.index:
        print('A: ' + str(A) + ' | submissionID: ' + str(submissionID))
        TitleDe = str(Articles.TitleDE[A]).strip()
        SubtitleDe = str(Articles.SubtitleDE[A]).strip()
        TitleEn = str(Articles.TitleEN[A]).strip()
        SubtitleEn = str(Articles.SubtitleEN[A]).strip()

        SectRef = SectDict[Articles.Section[A]]
        pages = Articles['Startpage'] + '-' + Articles['Endpage']
        Autoren = Articles.AuthorKey[A].split(', ')

        PDFfile = str(Articles.FilePDF[A])
        HTMLfile = str(Articles.FileHTML[A])
        supplementalHTMLfiles = str(Articles.supplementalFilesHTML[A])
        PDFfileTransl = MISSING_DATA  # str(Articles.FilePDF_Translation[A])
        HTMLfileTransl = MISSING_DATA  # str(Articles.FileHTML_Translation[A])

        if Articles.DOI[A] == MISSING_DATA or '':
            DOI = MISSING_DATA
            try:
                flash( str(A + 1).zfill(2) + ' keine DOI' )
            except:
                print( str(A + 1).zfill(2) + ': keine DOI' )
        elif str( Articles.DOI[A].strip() ) == 'ok':
            DOI = DoiGen(Vol, Nr, Articles.Startpage[A], DOIliste, DOIpre)
            print( str(A + 1).zfill(2) + ': ' + DOI + ' generiert' )
        elif str( Articles.DOI[A].split(':')[0] ) == 'DOI':
            DOI = Articles.DOI[A].split(':')[1]
            DOIliste.add(DOI)
            print( str(A + 1).zfill(2) + ': ' + DOI + ' manuell - mit DOI:' )
        else:
            DOI = Articles.DOI[A]
            DOIliste.add(DOI)
            print( str(A + 1).zfill(2) + ': ' + DOI + ' manuell - ohne DOI:' )
    
        try:
            keywordsDE = Articles.KeywordsDE[A].split(', ')
            keywordsEN = Articles.KeywordsEN[A].split(', ')
        except:
            print( A, 'no keywords' )
            keywordsDE = []
            keywordsEN = []

        abstractDE = Articles.AbstractDE[A]
        abstractEN = Articles.AbstractEN[A].replace('&mdash;',' - ')

        try:
            if Articles.Language[A] in ['Englisch', 'English', 'en', 'EN', 'En']:
                articleLanguage = 'en_US'
            else:
                articleLanguage = 'de_DE'
        except:
            articleLanguage = 'de_DE'

        CopyrightholderDE = CopyrightholderStrDE( Articles.copyrightHolder[A], VNLNDictDE )
        CopyrightholderEN = CopyrightholderStrEN( Articles.copyrightHolder[A], VNLNDictEN )

        if Articles.Startpage[A] == Articles.Endpage[A]:
            pages = Articles.Startpage[A]
        else:
            pages = Articles.Startpage[A] + '-' + Articles.Endpage[A]

        ArtNr = str(A + 1)

        submissionID = article( Authors, TitleDe, TitleEn, SubtitleDe, SubtitleEn,
                 DatePub, articleLanguage, SectRef, ArtNr,
                 DOI,
                 abstractDE, abstractEN,
                 keywordsDE, keywordsEN,
                 pages,
                 CopyrightholderDE, CopyrightholderEN,
                 Autoren,
                 Year, submissionID, 
                 PDFfile, HTMLfile, supplementalHTMLfiles, PDFfileTransl, HTMLfileTransl, URLpre, 
                 xml)
        outputArt += [ (str(A + 1).zfill(2), pages, articleLanguage, DOI, TitleDe[:100]) ]
    PostArticles(xml)
    xml.close()

    output = '\nDOIs: ' + str( len(DOIliste) ) + ' | Artikel: ' + str( len(Articles.index) )
    
    return output, outputArt, DOIliste



# ------------------------ #
# --- CLI mode         --- #
# ------------------------ #

def getCLIArguments( argv ):
    parser = argparse.ArgumentParser( description = 'Converts a special Excel file (XSLX) to OJS specific XML for importing a new issue with all article metadata, PDF, and HTML files for the TATuP journal (https://www.tatup.de).' )
    parser.add_argument( 'inputFile', help = 'name of input file' )
    parser.parse_args()
    args = parser.parse_args()
    if args.inputFile != '':
        return args.inputFile


def main( filename ):
    Journal, Articles, Authors, Dropdown = ExcelImport( filename )

    AuthorKeys = Articles.AuthorKey
    AutorC = AutorCheck( AuthorKeys, Authors )
    print( AutorC )

    XMLFileName = filename.rsplit( '.', 1 )[0] + '.xml'

    output, outputArt, DOIliste = XMLersteller( Journal, Articles, Authors, Dropdown, XMLFileName )

    print( output )
    for i in outputArt:
        print( i )


if __name__ == "__main__":
    inputFile = getCLIArguments( sys.argv[1:] )
    if inputFile != '':
        main( inputFile )
    else:
        print('No valid inputFile provided.')
