# OJS Converter

Converts a special Excel file (XSLX) to OJS specific XML for importing a new issue with all article metadata, PDF, and HTML files for the TATuP journal (https://www.tatup.de).

It provides a web and a CLI version.


## Dependencies

The following `Python 3` packages are used (install via `pip`):

- pandas: `pip3 install pandas`
- openpyxl: `pip3 install openpyxl`