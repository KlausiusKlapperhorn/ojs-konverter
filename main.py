from flask import Flask, flash, request, redirect, render_template, url_for, send_from_directory
from werkzeug.utils import secure_filename
import os
import OJSconverter as OJS
import pandas as pd
from flask.helpers import send_file


app = Flask (__name__)
app.secret_key = 'khalsdkfhoaiwen;lkasdfna;oweinsaklnfd'

app.config['UPLOAD_FOLDER'] = "uploads/"
app.config['XML_FOLDER'] = "XMLs/"

output = ''
outputArt = ''
AutorC = ''



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() == "xlsx"


@app.route('/uploads/<filename>')
def openConverter(uploadsF,filename):
        #filename = "OJS_Import_Vorlage_Archiv.xlsx" 
    try:
        print('kukuk')
        Journal, Articles, Authors, Dropdown = OJS.ExcelImport(uploadsF + filename)
        AutorC = OJS.AutorCheck(Articles.AuthorKey, Authors)
        return Journal, Articles, Authors, Dropdown, AutorC

    except:
        print('Import kaputt')
        X = 'huhu'
        flash('Import kaputt')
 

    
@app.route('/XMLs/<filename>')
def openConverterXML(filename, Journal, Articles, Authors, Dropdown):
    try:
        XMLfilename = app.config['XML_FOLDER'] + filename.rsplit('.',1)[0] + '.xml'
        output, outputArt, DOIliste = OJS.XMLersteller(Journal, Articles, Authors, Dropdown, XMLfilename)
        return XMLfilename, output, outputArt, DOIliste
    except:
        X='huhu'
        flash('kaputt')



@app.route('/download/<XMLfilename>')
def download(XMLfilename):
    print('huhu', XMLfilename)
    XMLFileName = XMLfilename.rsplit('.', 1)[0]+'.xml'
    
    return send_from_directory(app.config['XML_FOLDER'], XMLFileName, as_attachment = True, cache_timeout=0)



@app.route('/', methods=['GET', 'POST'])
def upload_file():
    

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('not a valid file.')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No file selected.')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            
            filename = secure_filename(file.filename)
            flash(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            #Rufe hier den Converter auf
            try:
                Journal,Articles,Authors,Dropdown,AutorC=openConverter(app.config['UPLOAD_FOLDER'], filename)
                print(AutorC)
                flash(AutorC)
            except:
                flash('Import failed: Error in the structure of the Excel file?')

            try:
                XMLfilename,output,outputArt,DOIliste=openConverterXML(filename,Journal,Articles,Authors,Dropdown)

                flash(output)
                for i in DOIliste:
                    flash(i)
                for i in outputArt:
                    flash(i)
            except:
                flash('Too bad. Something went wrong.')
            
            # flash(AutorC)
            #render_template('index.html')
            #return render_template('index.html')

            #return redirect(url_for('uploaded_file', XMLfilename=XMLfilename)) 

            

    return render_template('index.html')


if __name__=="__main__":
    from waitress import serve
    serve(app,host='0.0.0.0',port=5000) 

    #app.run(debug=True)